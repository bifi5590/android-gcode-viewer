#include "avb_gcode_opengl11_nativeLib_NativeLib.h"

JNIEXPORT jobject JNICALL Java_avb_gcode_opengl11_nativeLib_NativeLib_allocNativeBuffer(JNIEnv *env, jobject obj, jlong size) {
    //This function allocates the native buffer. This is much faster than the java version.
    //This also avoids the OutOfMemory bug in dalvik virtual machine when doing large allocations.
    void* buffer = malloc(size);
    jobject directBuffer = (*env)->NewDirectByteBuffer(env, buffer, size);
    jobject globalRef = (*env)->NewGlobalRef(env, directBuffer);
    return globalRef;

}

JNIEXPORT void JNICALL Java_avb_gcode_opengl11_nativeLib_NativeLib_freeNativeBuffer(JNIEnv *env, jobject obj, jobject globalRef) {
    //This releases the memory which is also not possible in Java. 
    void *buffer = (*env)->GetDirectBufferAddress(env,globalRef);
    (*env)->DeleteGlobalRef(env,globalRef);
    free(buffer);

}

