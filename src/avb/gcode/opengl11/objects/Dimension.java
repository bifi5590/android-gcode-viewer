package avb.gcode.opengl11.objects;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import javax.microedition.khronos.opengles.GL10;

import avb.gcode.opengl11.nativeLib.NativeLib;
import avb.gcode.process.DataStorage;

/**
 * The Class Dimension is used to display markers for object dimensions.
 * @author Alexander von Birgelen
 */
public class Dimension {
	
	/** The dimension constant. */
	public static final float DIMENSION_CONSTANT = 5.0f;
	
	/** The vertices. */
	private float vertices[];
	/** The order to connect the vertices. */
	private short[] indices;
	/** The colors. */
	private float colors[];
	/** The vertex buffer. */
	private FloatBuffer vertexBuffer;
	/** The index buffer. */
	private ShortBuffer indexBuffer;
	/** The color buffer. */
	private FloatBuffer colorBuffer;
	
	/** The number of indices. */
	private int noIndices = 0;
	

	
	/**
	 * Create buffers for OpenGL.
	 */
	public Dimension() {

		ByteBuffer vbb = null;
		ByteBuffer ibb = null;
		ByteBuffer cbb = null;
		
		NativeLib nl = new NativeLib();
		
		float minx = DataStorage.getMinimalX();
		float maxx = DataStorage.getMaximalX();
		float miny = DataStorage.getMinimalY();
		float maxy = DataStorage.getMaximalY();
		float minz = DataStorage.getMinimalZ();
		float maxz = DataStorage.getMaximalZ();
		
		try {
			//create vertices, indices and colors
			this.vertices = new float[18*3];	
			//*3 because X,Y and Z are needed
			
			this.indices = new short[18];
			
			this.colors = new float[18*4]; 
			//*4 because red,green,blue and alpha are needed for every vertice
			
			
			//X:
			vertices[0] = minx;
			vertices[1] = miny;
			vertices[2] = 0;
			
			vertices[3] = maxx;
			vertices[4] = miny;
			vertices[5] = 0;
			
			vertices[6] = minx;
			vertices[7] = miny-DIMENSION_CONSTANT;
			vertices[8] = 0;
			
			vertices[9] = minx;
			vertices[10] = miny+DIMENSION_CONSTANT;
			vertices[11] = 0;
			
			vertices[12] = maxx;
			vertices[13] = miny-DIMENSION_CONSTANT;
			vertices[14] = 0;
			
			vertices[15] = maxx;
			vertices[16] = miny+DIMENSION_CONSTANT;
			vertices[17] = 0;
			
			//Y
			vertices[18] = maxx;
			vertices[19] = miny;
			vertices[20] = 0;
			
			vertices[21] = maxx;
			vertices[22] = maxy;
			vertices[23] = 0;
			
			vertices[24] = maxx-DIMENSION_CONSTANT;
			vertices[25] = miny;
			vertices[26] = 0;
			
			vertices[27] = maxx+DIMENSION_CONSTANT;
			vertices[28] = miny;
			vertices[29] = 0;
			
			vertices[30] = maxx-DIMENSION_CONSTANT;
			vertices[31] = maxy;
			vertices[32] = 0;
			
			vertices[33] = maxx+DIMENSION_CONSTANT;
			vertices[34] = maxy;
			vertices[35] = 0;
			
			//Z
			vertices[36] = maxx;
			vertices[37] = miny;
			vertices[38] = minz;
			
			vertices[39] = maxx;
			vertices[40] = miny;
			vertices[41] = maxz;
			
			vertices[42] = maxx-DIMENSION_CONSTANT;
			vertices[43] = miny;
			vertices[44] = minz;
			
			vertices[45] = maxx+DIMENSION_CONSTANT;
			vertices[46] = miny;
			vertices[47] = minz;
			
			vertices[48] = maxx-DIMENSION_CONSTANT;
			vertices[49] = miny;
			vertices[50] = maxz;
			
			vertices[51] = maxx+DIMENSION_CONSTANT;
			vertices[52] = miny;
			vertices[53] = maxz;
			
			for(short i=0;i<indices.length;i++) {
				indices[i] = i;
			}
			
			for(int j=0;j<colors.length;j=j+4) {
				colors[j] = 1.0f;	//RED
				colors[j+1] = 1.0f;//GREEN
				colors[j+2] = 1.0f;	//BLUE
				colors[j+3] = 1.0f;//ALPHA
			}
			
			///////////////////////////////////////
			
			noIndices = indices.length;
			
			//create buffers:
			// float is 4 bytes so multiply the number of vertices with 4.
			vbb = nl.allocNativeBuffer(vertices.length * 4);
			vbb.order(ByteOrder.nativeOrder());
			vertexBuffer = vbb.asFloatBuffer();
			vertexBuffer.put(vertices);
			vertexBuffer.position(0);
			vertices = null;
			
			// short is 2 bytes so multiply the number of indices with 2.
			ibb = nl.allocNativeBuffer(indices.length * 2);
			ibb.order(ByteOrder.nativeOrder());
			indexBuffer = ibb.asShortBuffer();
			indexBuffer.put(indices);
			indexBuffer.position(0);
			indices = null;

			// float is 4 bytes so multiply the number of colors with 4.
			cbb = nl.allocNativeBuffer(colors.length * 4);
			cbb.order(ByteOrder.nativeOrder());
			colorBuffer = cbb.asFloatBuffer();
			colorBuffer.put(colors);
			colorBuffer.position(0);
			colors = null;
			
		}catch(OutOfMemoryError e) {
			nl.freeNativeBuffer(vbb);
			nl.freeNativeBuffer(ibb);
			nl.freeNativeBuffer(cbb);
			throw e;	//Throw Exception all the way back to the Activity to be able to send a Toast.
		}
	}

	/**
	 * Draw the object.
	 *
	 * @param gl The OpenGL data
	 */
	public void draw(GL10 gl) {
		// Counter-clockwise winding.
		gl.glFrontFace(GL10.GL_CCW);
		// Enabled the vertices buffer and the color array
		gl.glEnableClientState(GL10.GL_COLOR_ARRAY);
		// Specifies the location and data format of an array of vertex
		// coordinates to use when rendering.
		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);

		// Point out the where the color buffer is.
		gl.glColorPointer(4, GL10.GL_FLOAT, 0, colorBuffer);
		
		//Draw Elements
		gl.glDrawElements(GL10.GL_LINES, noIndices, 
				GL10.GL_UNSIGNED_SHORT, indexBuffer);

		// Disable the vertices and color buffer.
		gl.glDisableClientState(GL10.GL_COLOR_ARRAY);

	}
	
}
