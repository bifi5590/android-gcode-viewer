package avb.gcode.opengl11;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
//import android.widget.Toast;
import avb.gcode.R;
import avb.gcode.process.DataStorage;

/**
 * The Class Visualization3D is the activity that controls the 3d visualization 
 * with OpenGL.
 * @author Alexander von Birgelen
 */
public class Visualization3D extends Activity implements OnClickListener {
	
	/** The layer. */
	private int layer = 0;
	
	/** The maximum layer. */
	private int maxlayer = 0;
	
	/** The view. */
	private OpenGLSurfaceView view;
	
	/** The button to go down one layer. */
	private Button lowerlayer;
	
	/** The button to go up one layer. */
	private Button upperlayer;
	
	/** The layer text. */
	private TextView layers;
	
    /**
     * Called when the activity is first created.
     *
     * @param savedInstanceState the saved instance state
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	
    	DataStorage.setMemoryexception(false);
    	//Get number of layers
    	maxlayer = DataStorage.getLayers().size()-1;
    	layer = maxlayer;
    	
    	//Prepare fullscreen mode
    	this.requestWindowFeature(Window.FEATURE_NO_TITLE); 
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        //Set Surface View
        view = (OpenGLSurfaceView) findViewById(R.id.paint3d);
        
 		try {
			view = new OpenGLSurfaceView(this);
			setContentView(R.layout.visu3d);
			
			upperlayer = (Button) findViewById(R.id.upperlayer);
	    	upperlayer.setOnClickListener(this);
	    	lowerlayer = (Button) findViewById(R.id.lowerlayer);
	    	lowerlayer.setOnClickListener(this);
	    	layers = (TextView) findViewById(R.id.layers);
	    	layers.setText(layer+"/"+maxlayer);
			
 		}catch(OutOfMemoryError e) {
 			view = null; //try to kill old data
			e.printStackTrace();
			DataStorage.setMemoryexception(true);
			this.finish();
		}catch(Exception e) {
			view = null; //try to kill old data
			e.printStackTrace();
			Toast.makeText(this, e.toString(), Toast.LENGTH_LONG);
			this.finish();
		}
    	
    }

	/** 
	 * Handles click events.
	 * @param argo0 The view on which the event was performed 
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 */
	public void onClick(View arg0) {
		//Go up or down one layer
		if(arg0 == upperlayer && layer < maxlayer) {
			layer++;
		}
		if(arg0 == lowerlayer && layer > 1) {
			layer--;
		}
		
		layers.setText((layer)+"/"+maxlayer);
		view.renderer.setToLayer(layer+1);
		
	}
	   
}