package avb.gcode;

import java.io.File;

/**
 * The Class FileItem contains information about an item in a directory
 * @author Alexander von Birgelen
 */
public class FileItem implements Comparable<FileItem> {
	
	/** The file. */
	private File file;
	
	/** The true when this directory is parent directory. */
	private boolean isParent;
	
	
	/**
	 * Instantiates a new file item.
	 *
	 * @param file the file
	 */
	public FileItem(File file) {
		this.file = file;
	}
	
	/**
	 * Instantiates a new file item.
	 *
	 * @param file the file
	 * @param isParent the is parent
	 */
	public FileItem(File file,boolean isParent) {
		this.file = file;
		this.isParent = isParent;
	}

	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		if(!isParent) {
			return file.getName();
		}else{
			return "..";
		}
	}
	
	/**
	 * Gets the path.
	 *
	 * @return the path
	 */
	public String getPath() {
		return file.getAbsolutePath();
	}
	
	/**
	 * Gets the parent directory.
	 *
	 * @return the parent directory
	 */
	public File getParent() {
		return file.getParentFile();
	}
	
	/**
	 * Gets the data.
	 *
	 * @return the data
	 */
	public String getData() {
		if(!isDirectory()) {
			return String.valueOf(file.length());
		}else{
			if(!isParent) {
				return "Directory";
			}else{
				return "Parent Directory";
			}
		}
	}
	
	/**
	 * Checks if is directory.
	 *
	 * @return true, if is directory
	 */
	public boolean isDirectory() {
		return file.isDirectory();
	}


	/** Compare to interface
	 * @param f The fileItem to compare to
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(FileItem f) {
		
		if(this.getName() != null)
			//compare - case insensitive
            return this.getName().toLowerCase().compareTo(
            			f.getName().toLowerCase()); 
        else 
            throw new IllegalArgumentException();

	}
	
}
