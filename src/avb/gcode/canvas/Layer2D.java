package avb.gcode.canvas;

import java.util.ArrayList;
import android.content.Context;
import android.util.AttributeSet;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Path;
import android.view.View;
import avb.gcode.process.Coordinates;
import avb.gcode.process.DataStorage;
import avb.gcode.process.Coordinates.Type;

/**
 * The Class Layer2D.
 * @author Alexander von Birgelen
 */
public class Layer2D extends View{
	
	/** The layer. */
	private int layer = 1;
    
	/** The arrow size. */
	private final int ARROW_CONSTANT = 4;
	
	/** Distance between markers. */
	private final int SCALE_CONSTANT = 10; //mm
	
    /** The scale factor. */
    public float scaleFactor = 1.0f;
    
    /** The x movement. */
    public float translateX;
    
    /** The y movement. */
    public float translateY;
    
    /** The Paint. */
    private Paint paint;
    
    /** The scalepaint. */
    private Paint  scalepaint;
    
    /** The coordinates. */
    private ArrayList<Coordinates> coord;
    
    /**
     * Instantiates a new layer.
     *
     * @param context the context
     * @param attrs the attrs
     */
    public Layer2D(Context context,AttributeSet attrs) {
        super(context,attrs);  
        translateX = this.getWidth()/2;		//init default data
        translateY = this.getHeight()/2;
        
        paint = new Paint();
        paint.setStrokeWidth(2);
        
        scalepaint = new Paint();
        scalepaint.setStrokeWidth(1);
    }
    
    /**
     * Sets the layer.
     *
     * @param layer the new layer
     */
    public void setLayer(int layer) {
    	this.layer = layer;
    	//load layer data
    	coord = DataStorage.getLayers().get(layer).coordinates;
    	this.invalidate();	//repaint
    }
    
    /**
     * Paint the layer.
     *
     * @param canvas the canvas
     */
    private void paintLayer(Canvas canvas) {
    	
    	if(coord == null) {
    		//fix for null pointer exception graphical view designer
    		coord = new ArrayList<Coordinates>();
    	}else{
    	
    		float z = 0;
	        
	    	float lX=0; // l = last
	    	float lY=0; 
	    	float tX=0;
	    	float tY=0;

	        boolean firstpoint = true;

	        //Go through every command in the layer
	        for(Coordinates c : coord) {
	            if (c.layer == this.layer) {
	                if(firstpoint) {
	                    //set new x,y to last x,y 
	                    tX = c.X;
	                    tY = c.Y;
	                    
	                    lX = tX;
	                    lY = tY;
	                    
	                    firstpoint = false;
	                    
	                }else{
	                    z = c.Z;    //Get Z layer
	
	                    tX = c.X;	//target values
	                    tY = c.Y;
	                    
	                    //set the color
	                    if(c.type == Type.UNKNOWN) {
	                    	paint.setColor(Color.GRAY);
	                    }
	                    if(c.type == Type.MOVE) {
	                    	paint.setColor(Color.LTGRAY);
	                    }
	                    if(c.type == Type.SKIRT) {
	                    	paint.setColor(Color.BLUE);
	                    }
	                    if(c.type == Type.PERIMETER) {
	                    	paint.setColor(Color.RED);
	                    }
	                    if(c.type == Type.FILL) {
	                    	paint.setColor(Color.GREEN);
	                    }
	                    if(c.type == Type.BRIDGE) {
	                    	paint.setColor(Color.YELLOW);
	                    }
	                    
	                    drawArrow(canvas, lX*scaleFactor, (-1)*lY*scaleFactor,
	                    		  tX*scaleFactor, (-1)*tY*scaleFactor);
	                    
	                    //set new x,y to last x,y 
	                    lX = tX;
	                    lY = tY;
	                }
	            }
	        }
	        
	        paintCoordinates(canvas,z);	//paint the z height
	        
	        
    	}
    }
    
    /**
     * Paint Z coordinates.
     *
     * @param canvas the canvas
     * @param z the z
     */
    private void paintCoordinates(Canvas canvas, double z) {
    	canvas.restore();
    	Paint pinsel = new Paint();
        pinsel.setColor(Color.GRAY);
        pinsel.setStrokeWidth(1);
        pinsel.setTextAlign(Align.RIGHT);
        
    	canvas.drawText(Math.round(z*1000)/1000.0+"mm", this.getWidth(), 20,
    					pinsel);
    }
    
    /**
     * Paint a coordinate system.
     *
     * @param canvas the canvas
     */
    private void paintDimensions(Canvas canvas) {
    	
    	scalepaint.setColor(Color.WHITE);
    	paint.setColor(Color.WHITE);
    	
    	this.drawArrow
    		(	
    			canvas, 
    			(DataStorage.getMinimalX() - ARROW_CONSTANT) * scaleFactor,
    			0, 
    			(DataStorage.getMaximalX()+ ARROW_CONSTANT) * scaleFactor,
    			0
    		);
    	this.drawArrow
    		(	
    			canvas,
    			0,
    			(-1) * (DataStorage.getMinimalY() -ARROW_CONSTANT) *scaleFactor,
    			0, 
    			(-1)*(DataStorage.getMaximalY() + ARROW_CONSTANT) * scaleFactor
    		);

    	for(int x = ((int)DataStorage.getMinimalX()/SCALE_CONSTANT); 
    		x <= ((int)DataStorage.getMaximalX()/SCALE_CONSTANT);x++) 
    	{
    		canvas.drawLine(
    				((float)x*(float)SCALE_CONSTANT)*scaleFactor,
    				-ARROW_CONSTANT,
    				((float)x*(float)SCALE_CONSTANT)*scaleFactor, 
    				ARROW_CONSTANT, 
    				scalepaint
    		);
    		canvas.drawText(
    				Float.toString(x*SCALE_CONSTANT), 
    				((float)x*(float)SCALE_CONSTANT)*scaleFactor, 
    				-ARROW_CONSTANT, 
    				scalepaint
    		);
    	}
    	for(int y = ((int)DataStorage.getMinimalY()/SCALE_CONSTANT); 
    		y <= ((int)DataStorage.getMaximalY()/SCALE_CONSTANT);y++) 
    	{
    		canvas.drawLine(
    				-ARROW_CONSTANT,
    				((float)y*(float)SCALE_CONSTANT)*scaleFactor, 
    				-ARROW_CONSTANT, 
    				((float)y*(float)SCALE_CONSTANT)*scaleFactor, 
    				scalepaint
    		);
    		if(y != 0) {
    			canvas.drawText(
    					Float.toString(y*SCALE_CONSTANT), 
    					-ARROW_CONSTANT, 
    					((float)y*(float)SCALE_CONSTANT)*scaleFactor, 
    					scalepaint
    			);
    		}
    	}
    	
    }
    
        
    /**
     * Called when Runtime needs to repaint.
     * @param canvas The canvas.
     * @see android.view.View#onDraw(android.graphics.Canvas)
     */
    @Override protected void onDraw(Canvas canvas) {
    	super.onDraw(canvas);
    	
    	canvas.save();    	
    	canvas.translate(translateX, translateY);
    	//canvas.scale(scaleFactor, scaleFactor);	
    	//scales also the width of the lines so this is useless and the scaling
    	//has to be applied to every point
    	paintDimensions(canvas);
    	paintLayer(canvas);
    	
    }
    
    /**
     * Draw arrow.
     *
     * @param canvas the canvas
     * @param startX the start x
     * @param startY the start y
     * @param endX the end x
     * @param endY the end y
     */
    private void drawArrow(Canvas canvas, float startX, float startY, 
    						float endX, float endY) {
    	
    	//Draw a line
    	canvas.drawLine(startX, startY, endX, endY, paint);
    	
    	//Draw the Arrow tip
    	float dx = (endX-startX);
    	float dy = (endY-startY);
    	double l = Math.sqrt(dx*dx+dy*dy);
    	if(l == 0) {	//a movement only? --> retract return only
    		return;
    	}
    	
    	//Calculate the angle for arrow tip painting
    	double alpha = Math.asin(dy/l)+Math.PI/2;
    	if(dx < 0) {
    		alpha = -alpha;
    	}
    	
    	Path arrowtip = new Path();
    	Matrix m = new Matrix();
    	m.setTranslate(endX, endY);
    	
    	arrowtip.moveTo(0,0);
    	//Apply a rotation matrix manually because m.setRotate has no effect!
    	//(Xneu)   (cos a    -sina )   (Xalt)
    	//(Yneu) = (sin a     cosa ) * (Yalt)
    	arrowtip.lineTo
    	(
    		(float)
    		(Math.cos(alpha)*-ARROW_CONSTANT-Math.sin(alpha)*ARROW_CONSTANT),
    		(float)
    		(Math.sin(alpha)*-ARROW_CONSTANT + Math.cos(alpha)*ARROW_CONSTANT)
    	);
    	arrowtip.lineTo
    	(
    		(float)
    		(Math.cos(alpha)*ARROW_CONSTANT-Math.sin(alpha)*ARROW_CONSTANT),
    		(float)
    		(Math.sin(alpha)*ARROW_CONSTANT + Math.cos(alpha)*ARROW_CONSTANT)
    	);
    	arrowtip.close();
    	arrowtip.transform(m);	//apply transformation
    	
    	canvas.drawPath(arrowtip, paint);	//paint arrow tip
    	
    }
    
    
}
