package avb.gcode.process;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import avb.gcode.canvas.Visualization2D;
import avb.gcode.opengl11.Visualization3D;
import avb.gcode.process.Coordinates.Type;
import avb.gcode.R;
import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

/**
 * The Class ProcessGCode reads the G-Code and saves it in DataStorage.
 * @author Alexander von Birgelen
 */
public class ProcessGCode extends Activity implements Runnable, 
														  OnClickListener {
	
	/** The Constant TAG. */
	private static final String TAG = "OpenGLRenderer"; //For debug only
	
	/** The file. */
	private File file;
    
    /** The Layers. */
    private ArrayList<Layer> data;
    
    /** The status text. */
    private String statusText;
    
    /** The progress. */
    private int progress;
    
    /** The info test. */
    private String info;
    
    /** The path to the file. */
    private String filepath;
    
	/** The travel distance. */
	private float distance;
	
	/** The execution time. */
	private float executiontime;
	
	/** The layer height. */
	private float layerheight;
    
    /** The dimensions. */
    private float minx, maxx, miny, maxy, minz, maxz, maxf;
    
    /** Help variables for the last values. */
    private float lx, ly, lz, lf;
    
    /** The layer. */
    private int layer;  
    
    /** Indicator for work in progress. */
    private boolean working;
	
    /** The progress text. */
    private TextView progressText;
    
    /** The progress bar. */
    private ProgressBar progressBar;
    
    /** The filename view. */
    private TextView filename;
    
    /** The information. */
    private EditText information;
    
    /** The button to start 2D visualization. */
    private Button showlayers;
    
    /** The button to start 3D visualization. */
    private Button show3d;
    
    /** Reference to this Activity for Handler. */
     private ProcessGCode selfref;
     
     /** This is the thread that does the calculation. */
     private Thread thread;
     
    /**
     * Called when Activity is created.
     * @param savedInstanceState The instance state
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	
    	selfref = this;
    	
    	info = "Calculating";	//set info text
    	
    	initGUI();	//initialize the GUI

    	//Get the file from Intent
    	if(getIntent().getAction().equals("android.intent.action.VIEW")) {
    		//Called by a file manager.
    		Log.d(TAG, "URI");
    		String path = getIntent().getDataString();
    		filepath = path.replace("file://", "");
    	}else{
    		//called by GCodeViewerActivity
    		Log.d(TAG, "FILE");
    		filepath = getIntent().getExtras().getString("File");
    	}
    	
    	//Log message for debug during development
    	Log.d(TAG,getIntent().getAction());	
    	
    	progressText = (TextView) findViewById(R.id.progresstext);
    	progressText.setText(filepath);
    	
    	
    	//Try to open the file
    	try {
    		Log.d(TAG, "Data:" + filepath);
    		file = new File(filepath);
    	}catch(Exception e) {
    		//File does not exist or missing permissions
    		filename.setText("File can't be read.");
    		e.printStackTrace();
    		return;
    	}
    	
    	//Init data
    	data = new ArrayList<Layer>();
    	data.add(new Layer());
    	
    	//Disable controls
    	showlayers.setEnabled(false);
    	showlayers.setClickable(false);
    	show3d.setEnabled(false);
    	show3d.setClickable(false);
    	
    	if(file.exists() && file.canRead()) {
    		
    		//File ok. Start processing.
    		
    		filename.setText(file.getName());
    		
    		thread = new Thread(this);
    		thread.start();

    	}else{
    		filename.setText("File can't be read.");
    	}
    	
    }
    
    
    
    /**
     * Inits the gui.
     */
    private void initGUI() {
    	
    	setContentView(R.layout.file_progress);
    	
    	information = (EditText) findViewById(R.id.information);
    	information.setText(info);
    	information.setClickable(false);
    	information.setFreezesText(true);
    	information.setFocusable(false);
    	information.setLongClickable(false);

    	progressText = (TextView) findViewById(R.id.progresstext);
    	progressText.setText(statusText);
    	
    	progressBar = (ProgressBar) findViewById(R.id.progressbar);
    	progressBar.setProgress(progress);
    	
    	showlayers = (Button) findViewById(R.id.showlayers);
    	showlayers.setOnClickListener(this);
    	
    	show3d = (Button) findViewById(R.id.show3d);
    	show3d.setOnClickListener(this);
    	
    	filename = (TextView) findViewById(R.id.filename);
    	if(file != null) {
    		filename.setText(file.getName());
    	}
    	
    }
    
    /**
     * Called on mobile phone rotation or keyboard change.
     * @param  newConfig The new configuration.
     * @see android.app.Activity#onConfigurationChanged(android.content.res.Configuration)
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
      super.onConfigurationChanged(newConfig);
      initGUI();
      handler.sendEmptyMessage(0); //Set old state of the controls
      
      if(DataStorage.isMemoryexception()) {
    	 handler.sendEmptyMessage(0);
      }
      
    }
    
    //@Override
    /** 
	 * Process the file.
     * @see java.lang.Runnable#run()
     */
    public void run() {
        
        String line;
        int lineno = 0;
        working = true;
        
        try {
        	//Read lines from the file
            BufferedReader reader = new BufferedReader(new FileReader(file));
            
            int lines = 0;
            //Get number of lines in file
            while(reader.readLine() != null) {
                lines++;
            }
            
            reader = new BufferedReader(new FileReader(file));
            while((line = reader.readLine()) != null) {
            	
            	//Extract data from line
            	try {
            		this.calculate(line);
            	}catch(OutOfMemoryError e) {
            		thread.destroy();
            		System.gc();
            		e.printStackTrace();
            		
            	}
                
                //Set status text
                statusText = "Line "+lineno+"/"+lines;
                progress = (int) ((100.0/(double)lines)*(double)lineno);
                handler.sendEmptyMessage(0); //Use a message handler in thread 
                							 //to update GUI
                
                lineno++;
            }
              
            reader.close();
            
            //Processing finished. 
            //Set Status messages in GUI and set global variables as well
            
            DataStorage.setMaximalX(maxx);
            DataStorage.setMaximalY(maxy);
            DataStorage.setMaximalZ(maxz);
            DataStorage.setMinimalX(minx);
            DataStorage.setMinimalY(miny);
            DataStorage.setMinimalZ(minz);
            
            
            statusText = "Line "+lineno+"/"+lines;
            progress = 100;
            if(layer > 0) {
	            info = "X: "+minx+" - "+maxx+
	    				  "mm\nY: "+miny+" - "+maxy+
	    				  "mm\nZ: "+minz+" - "+maxz+" mm("+(layer)+" * "+
	    				  Math.round(layerheight*100)/100.0+"mm)"+
	    				  "\nMoving "+(int)distance+"mm in "+
	    				  Math.round(executiontime*10)/10.0+"min.";
            }else{
            	info = "The file does not contain compatible G-Code!";
            	progress = 0;
            }
            working = false;
            handler.sendEmptyMessage(0);    
          
        }catch(Exception E) {
            System.out.println(E.getMessage());
            E.printStackTrace();
        }
        
    }
    
    /** The message handler is used to update the gui data from a thread. */
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            if(progress == 100) {
            	showlayers.setEnabled(true);
            	showlayers.setClickable(true);
            	if(!DataStorage.isMemoryexception()) {
	            	show3d.setEnabled(true);
	            	show3d.setClickable(true);
            	}else {
            		show3d.setEnabled(false);
                	show3d.setClickable(false);
            	}
            }else{
            	showlayers.setEnabled(false);
            	showlayers.setClickable(false);
            	show3d.setEnabled(false);
            	show3d.setClickable(false);
            }
        	
        	if(DataStorage.isMemoryexception()) {
    			Toast.makeText(selfref, 
    						   "Not enough Memory for rendering object.", 
    						   Toast.LENGTH_LONG).show();
    			show3d.setEnabled(false);
    			
    			if(progress < 100) {
    				info ="Error loading file.";
    			}
    		}
        	
            progressText.setText(statusText);
            progressBar.setProgress(progress);
            if(!working) {
            	information.setText(info);
            }

        }
    };
    
 
    
    /**
     * Calculate the data out of the file.
     *
     * @param line the line
     */
    private void calculate(String line) {
        float X,Y,Z,F;	//Current X,Y,Z and F values
        X=Y=Z=F=0;
        
        if(line.startsWith("G1")) {
             //G1 command supported. Try and extract coordinates
             int xpos = line.indexOf("X");
             int xspace = line.indexOf(" ", xpos);
             
             int ypos = line.indexOf("Y");
             int yspace = line.indexOf(" ", ypos);
             
             int zpos = line.indexOf("Z");
             int zspace = line.indexOf(" ", zpos);
             
             int fpos = line.indexOf("F");
             int fspace = line.indexOf(" ", fpos);
             if (fspace == -1) {
                 fspace = line.length()-1;
             }

             //Extract X, use old value otherwise
             if(xpos == -1 || xspace == -1) {
                 X = lx;
             }else{
                X = Float.valueOf(line.substring(xpos+1, xspace));
             }
 
             //Extract Y, use old value otherwise
             if(ypos == -1 || yspace == -1) {
                 Y = ly;
             }else{
                Y = Float.valueOf(line.substring(ypos+1, yspace));
             }
             
             //Extract Z, use old value otherwise
             if(zpos == -1 || zspace == -1) {
                 Z = lz;
             }else{
                Z = Float.valueOf(line.substring(zpos+1, zspace));
             }

             //Extract F, use old value otherwise
             if(fpos == -1 || fspace == -1) {
                 F = lf;
             }else{
                 F = Float.valueOf(line.substring(fpos+1, fspace));
             }

             //Get distance for each axis
             float dx = Math.abs(lx-X);
             float dy = Math.abs(ly-Y);
             float dz = Math.abs(lz-Z);
             
             //we have a new layer when Z distance is not 0
             if(dz != 0) {
            	 layerheight = dz; //layerheight can be different for the first 
            	 //layers depending on slic3r configuration.
            	 //so just save it every time a new layer is started
             }
             
             float dist = (float) Math.sqrt(dx*dx+dy*dy+dz*dz);	
             //overall distance for current move
             distance = distance + dist;	//add to complete distance
             executiontime = executiontime + dist/F;	//estimate the time 
             											//for this move
             //this is rounded. Time changes depending on the machines 
             //acceleration and deceleration and path optimization.
             //EMC and other advanced can make corners round to save time, 
             //but this features can not be extracted from the G-code.
             
             	//Get dimensions for information.
                 if(X < minx) { minx=X; }
                 if(X > maxx) { maxx=X; }
                 if(Y < miny) { miny=Y; }
                 if(Y > maxy) { maxy=Y; }
                 if(Z < minz) { minz=Z; }
                 if(Z > maxz) { maxz=Z; }
                 if(F > maxf) { maxf=F; }
                 
                
           Coordinates coord = new Coordinates();
           
           if(lz < Z || lz > Z) {
               layer++;	//Add a new layer
               data.add(layer, new Layer());
           }
           
           coord.layer = layer;	
           
           //Set colors for movement
           if(line.contains("move")) {
        	   coord.type = Type.MOVE;
           }else if(line.contains("fill")) {
        	   coord.type = Type.FILL;
           }else if(line.contains("perimeter")) {
        	   coord.type = Type.PERIMETER;
           }else if(line.contains("retract")) {
        	   coord.type = Type.RETRACT;
           }else if(line.contains("compensate")) {
        	   coord.type = Type.COMPENSATE;
           }else if(line.contains("bridge")) {
        	   coord.type = Type.BRIDGE;
           }else if(line.contains("skirt")) {
        	   coord.type = Type.SKIRT;
           }else{
        	   coord.type = Type.UNKNOWN;
           }
           
           coord.X = (float)X;
           coord.Y = (float)Y;
           coord.Z = (float)Z;
           coord.F = (float)F;
           
           //add coordinates to data structure
           data.get(layer).coordinates.add(coord);
             
         //Update the last values  
         lx=X;
         ly=Y;
         lz=Z;
         lf=F;
           
        }else if(line.startsWith("M")) {
        	//Add some execution time when a Machine code is executed. 
        	//Machine codes are machine specific so just add a roundabout value
            executiontime += 0.016666667; //add half a second to time for mcode
        }else if(line.startsWith("G4")) {
        	//G4 is a wait command. So add wait time to execution time.
        	String time = line.replace("G4 P", "");
        	executiontime += Double.parseDouble(time)/60;
        }
    }
    
	/** Called when a click is performed.
	 * @param arg0 The view element on which the action was performed.
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 */
	public void onClick(View arg0) {

		//Store data
		DataStorage.putLayers(data); //Refresh data
		
		if(arg0 == showlayers) {
			//Show layers in 2 dimensional mode
			Intent intent = new Intent(this,Visualization2D.class);
			startActivity(intent);
		}
		
		if (arg0 == show3d) {
			//Show layers in 3D mode
			Intent intent = new Intent(this,Visualization3D.class);
			this.startActivityForResult(intent, 1);
		}
		
	}
	
	/** Result from Activity Visualization3D
	 * @param requestCode The request code
	 * @param resultCode The result code
	 * @param data The Intent
	 * @see android.app.Activity#onActivityResult(int, int, android.content.Intent)
	 */
	public void onActivityResult(int requestCode, int resultCode, 
								  Intent data) {
		if(DataStorage.isMemoryexception()) {
			System.gc();
			handler.sendEmptyMessage(0);
		}
	}

}
